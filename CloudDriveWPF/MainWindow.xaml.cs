﻿using CloudDrive.Services;
using System.Windows;

namespace CloudDriveWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private UserService _userService;

        public MainWindow()
        {
            InitializeComponent();

            _userService = new UserService();
        }

        private async void SignInButtonClick(object sender, RoutedEventArgs e)
        {
            var email = emailTextBox.Text;
            var password = passwordBox.Password;

            if (_userService.IsDataEmpty(email, password))
            {
                MessageBox.Show("Enter data");
                return;
            }

            if (await _userService.SignInAsync(email, password))
            {
                MessageBox.Show("Authentication successed!");
                new FilesWindow().filesWindow.Show();
                Close();
            }
            else
            {
                MessageBox.Show("Login or password isn't correct!");
            }
        }

        private async void SignUpButtonClick(object sender, RoutedEventArgs e)
        {
            var email = newUserEmailTextBox.Text;
            var password = newUserPasswordBox.Password;

            if (_userService.IsDataEmpty(email, password))
            {
                MessageBox.Show("Enter data");
                return;
            }

            if (await _userService.UserExistsAsync(email))
            {
                MessageBox.Show("User already exists");
                return;
            }

            if (await _userService.SignUpAsync(email, password))
            {
                MessageBox.Show("Congratulations! You've successfully signed up!");
                new FilesWindow().filesWindow.Show();
                Close();
            }
            else
            {
                MessageBox.Show("Error! Try again, please!");
            }
        }

        private void OpenSignUpDialogButtonClick(object sender, RoutedEventArgs e)
        {
            signUpDialog.IsOpen = true;
        }
    }
}
